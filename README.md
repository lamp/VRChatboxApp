# VRChatboxApp

- Shows time and FPS in chatbox (updates every 2 seconds)
- Shows the active application if it's not VRChat (useful when chilling on desktop while alt-tabbing to other things)
- Input chat with external window
	- Allows you to **continue chatting on desktop after taking off your headset and loosing control of VRChat**
	- Chat persists until cleared
	- Chat messages are appended so you have some **chat history**
	- Able to update chatbox instantly while avoiding spam timeout

![screenshot](screenshot.png)

Download latest build from [releases](https://gitea.moe/lamp/VRChatboxApp/releases) and just run the executable.

### Notes

- If it doesn't work, run it as administrator, or add your user to the "Performance Log Users" group: open "Computer Management", find your user, type in this exact group name, then log out and back in.
- This app uses VRChat OSC; make sure to [enable it](https://docs.vrchat.com/docs/osc-overview#enabling-it).
- You cannot use in-game chatbox input, hence the external window.
- Anti-spam-timeout isn't foolproof apparently; you might trigger it if you spam chat input of this app.
- The timeout may also trigger after VRChat freezes (such as when loading a world).


### Todo

- Osc receiver so that other stuff can be plugged into this app
- GUI improvements, typing indicator
- What other cool stuff could be put in the chatbox?
