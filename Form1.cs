﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VRChatboxApp {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
			VRChatboxApp.chatbox.OnSend += (o, e) => {
				label1.Invoke(new Action(() => {
					label1.Text = e.Value;
				}));
			};
		}

		private void button1_Click(object sender, EventArgs e) {
			VRChatboxApp.SendChatMessage(textBox1.Text);
			textBox1.Text = "";
		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				button1.PerformClick();
				e.SuppressKeyPress = true;
			}
		}
	}
}
